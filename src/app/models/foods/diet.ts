export class Diet {
    private _id: number;
    private _name: string;
    private _comment: string;

    constructor(id: number, name: string, comment: string){
        this._id = id;
        this._name = name;
        this._comment = comment;
    }

    public get id(): number {
        return this._id;
    }

    public get name(): string {
        return this._name;
    }

    public get comment(): string {
        return this._comment;
    }

    public set id(value: number) {
        this._id = value;
    }

    public set name(value: string) {
        this._name = value;
    }

    public set comment(value: string) {
        this._comment = value;
    }
}