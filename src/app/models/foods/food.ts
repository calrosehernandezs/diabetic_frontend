export class Food {
    private _id: number;
    private _name: string;
    private _description: string;
    private _comment: string;

    constructor(id: number, name: string, description: string, comment: string){
        this._id = id;
        this._name = name;
        this._description = description;
        this._comment = comment;
    }

    public get id(): number {
        return this._id;
    }

    public get name(): string {
        return this._name;
    }

    public get description(): string {
        return this._description;
    }

    public get comment(): string {
        return this._comment;
    }

    public set id(value: number) {
        this._id = value;
    }

    public set name(value: string) {
        this._name = value;
    }

    public set description(value: string) {
        this._description = value;
    }

    public set comment(value: string) {
        this._comment = value;
    }
}