export class Follow_up{
    private _id: number;
    private _userId: number;
    private _dietId: number;
    private _dateInitial: Date;
    private _dateEnd: Date;

    constructor(id: number, userId: number, dietId: number, dateInitial: Date, dateEnd: Date){
        this._id = id;
        this._userId = userId;
        this._dietId = dietId;
        this._dateInitial = dateInitial;
        this._dateEnd = dateEnd
    }
    
    public get id(): number {
        return this._id;
    }

    public set id(value: number) {
        this._id = value;
    }

    public get userId(): number {
        return this._userId;
    }

    public set userId(value: number) {
        this._userId = value;
    }

    public get dietId(): number {
        return this._dietId;
    }
    
    public set dietId(value: number) {
        this._dietId = value;
    }

    public get dateInitial(): Date {
        return this._dateInitial;
    }
    
    public set dateInitial(value: Date) {
        this._dateInitial = value;
    }

    public get dateEnd(): Date {
        return this._dateEnd;
    }

    public set dateEnd(value: Date) {
        this._dateEnd = value;
    }
}