export class Glucometer{
    private _id: number;
    private _userId: number;
    private _date: Date;
    private _value: number;
    private _comment: string;
    private _daily: string;
    private _moment: string;

    constructor(
        id: number, 
        userId: number, 
        date: Date, 
        value: number, 
        comment: string,
        daily: string,
        moment: string){
            this._id = id;
            this._userId = userId;
            this._date = date;
            this._value = value;
            this._comment = comment;
            this._daily = daily;
            this._moment = moment;
    }

    public get moment(): string {
        return this._moment;
    }
    
    public get daily(): string {
        return this._daily;
    }

    public get comment(): string {
        return this._comment;
    }

    public get value(): number {
        return this._value;
    }

    public get date(): Date {
        return this._date;
    }

    public get userId(): number {
        return this._userId;
    }

    public get id(): number {
        return this._id;
    }

    public set id(value: number) {
        this._id = value;
    }

    public set userId(value: number) {
        this._userId = value;
    }

    public set date(value: Date) {
        this._date = value;
    }

    public set value(value: number) {
        this._value = value;
    }

    public set comment(value: string) {
        this._comment = value;
    }

    public set daily(value: string) {
        this._daily = value;
    }

    public set moment(value: string) {
        this._moment = value;
    }
}