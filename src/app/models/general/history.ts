export class History{
    private _id: number;
    private _userId: number;
    private _date: Date;
    private _average: number;

    constructor(id: number, userId: number, date: Date, average: number){
        this._id = id;
        this._userId = userId;
        this._date = date;
        this._average = average;
    }

    public get average(): number {
        return this._average;
    }
    public set average(value: number) {
        this._average = value;
    }
    public get date(): Date {
        return this._date;
    }
    public set date(value: Date) {
        this._date = value;
    }
    public get userId(): number {
        return this._userId;
    }
    public set userId(value: number) {
        this._userId = value;
    }
    public get id(): number {
        return this._id;
    }
    public set id(value: number) {
        this._id = value;
    }
}