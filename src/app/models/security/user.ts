export class User{
    private _id: number;
    private _name: string;
    private _email: string;
    private _password: string;
    private _accountGoogle: string;
    private _status: string;

    /*constructor(id: number, name: string, email: string, password: string, accountGoogle: string, status: string){
        this._id = id;
        this._name = name;
        this._email = email;
        this._password = password;
        this._accountGoogle = accountGoogle;
        this._status = status;
    }*/

    constructor(){}

    public get status(): string {
        return this._status;
    }
    public set status(value: string) {
        this._status = value;
    }

    public get accountGoogle(): string {
        return this._accountGoogle;
    }
    public set accountGoogle(value: string) {
        this._accountGoogle = value;
    }

    public get password(): string {
        return this._password;
    }
    public set password(value: string) {
        this._password = value;
    }
    

    public get email(): string {
        return this._email;
    }
    public set email(value: string) {
        this._email = value;
    }

    public get name(): string {
        return this._name;
    }
    public set name(value: string) {
        this._name = value;
    }
    

    public get id(): number {
        return this._id;
    }
    public set id(value: number) {
        this._id = value;
    }
}