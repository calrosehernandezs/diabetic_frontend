import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from 'src/app/models/security/user';
import { environment } from 'src/environments/environment.prod';
import { catchError, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private API_URL: string;

  constructor(private httpClient: HttpClient) {
    this.API_URL = `${environment.apiHost}/${environment.apiName}`
  }

  postLogin(user: User): Observable<any> {
    return this.httpClient.post<User>(`${this.API_URL}/login/`, user)
      .pipe(
        catchError(this.handleError<User>('Error occured'))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }  
}
