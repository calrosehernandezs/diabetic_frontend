import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/security/user';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private email: string = "";
  private password: string = "";


  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  onSubmit() {
    const userCredential = new User();
    userCredential.email = this.email;
    userCredential.password = this.password;

    this.authService.postLogin(userCredential).subscribe(
      (resp: any) => {
        console.log(resp);
      },
      ({ error }) => {
        console.log(error);
      }
    );
  }

}
