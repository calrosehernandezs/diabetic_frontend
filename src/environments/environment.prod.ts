export const environment = {
  production: true,
  apiHost: 'http://localhost:8090',
  apiName: 'diabetic_backend',
};
